package cr.ac.ucenfotec.directoryclothing.ui;

import cr.ac.ucenfotec.directoryclothing.bl.Camiseta;
import cr.ac.ucenfotec.directoryclothing.bl.Cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static ArrayList<Camiseta>camisetas = new ArrayList<>();
    static ArrayList<Cliente>clientes = new ArrayList<>();

    public static void main(String[] args) throws IOException {

        int opcion = 0;
       do {
           opcion = pintarMenu();
           switch(opcion){

               case 1:agregarCamiseta();
               break;
               case 2:listarCamiseta();
               break;
               case 3: agregarCliente();
               break;
               case 4: listarCliente();
               break;
           }
       }while (opcion !=0);


    }//Find del main

    public static int pintarMenu() throws IOException {
        System.out.println("*** Bienvenido a Directoty Clothing, INC ***");
        System.out.println();
        System.out.println("--- MENU---");
        System.out.println("1. Agregar Camiseta");
        System.out.println("2. Listar Camiseta");
        System.out.println("3. Agregar Cliente");
        System.out.println("4. Listar Cliente");
        int opcion = Integer.parseInt(in.readLine());
        return opcion;
    }

    public static void agregarCamiseta()throws IOException{
        System.out.println("Digite el ID");
        System.out.println("Digite el color");
        System.out.println("Digite la talla");
        System.out.println("Digite la descripción ");
        System.out.println("Digite el precio");
        int id = Integer.parseInt(in.readLine());
        int color = Integer.parseInt(in.readLine());
        char talla = in.readLine().charAt(0);
        String descripcion = in.readLine();
        int precio = Integer.parseInt(in.readLine());
        Camiseta camiseta1;
        camiseta1 = new Camiseta(id,color,talla,descripcion,precio);
        camisetas.add(camiseta1);
    }

    public static void listarCamiseta() {
        for (int i = 0; i < camisetas.size(); i++) {
            Camiseta camiseta = camisetas.get(i);
            System.out.println(i + ") " + camiseta.toString());
            System.out.println("");
        }
    }
    public static void agregarCliente()throws IOException{
        System.out.println("Digite nombre");
        System.out.println("Digite primer apellido");
        System.out.println("Digite segundo apellido");
        System.out.println("Digite la dirección");
        System.out.println("Digite el correo electrónico");
        System.out.println("Digite el metodo de pago");
        String nombre = in.readLine();
        String primerApellido = in.readLine();
        String segundoApellido = in.readLine();
        String direccion = in.readLine();
        String correo = in.readLine();
        String metodoPago = in.readLine();
        Cliente cliente1 = new Cliente(nombre,primerApellido, segundoApellido, direccion, correo, metodoPago);
        boolean existe = false;
        for (int i =0; i < clientes.size(); i++) {
            if(clientes.get(i).equals(cliente1)){
                existe = true;
            }
        }
        if (!existe){
            clientes.add(cliente1);
        }
    }

    public static void listarCliente() {
        for (int i = 0; i < clientes.size(); i++) {
            Cliente cliente = clientes.get(i);
            System.out.println(i + ") " + cliente.toString());
            System.out.println("");
        }
    }


}//Fin de la clase
