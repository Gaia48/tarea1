package cr.ac.ucenfotec.directoryclothing.bl;

public class Camiseta {

    //Variables

    private int id;
    private int color;
    private char talla;
    private String descripcion;
    private int precio;

    //Constructor que no recibe parametros

    public Camiseta() {
        setId(0);
        setColor(0);
        setTalla('s');
        setDescripcion("");
        setPrecio(0);
    }


    //Constructor que recibe parametros

    public Camiseta(int id, int color, char talla, String descripcion, int precio) {
        setId(id);
        setColor(color);
        setTalla(talla);
        setDescripcion(descripcion);
        setPrecio(precio);

    }

    //Setter & Getters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public char getTalla() {
        return talla;
    }

    public void setTalla(char talla) {
        this.talla = talla;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Camiseta{" +
                "id=" + id +
                ", color=" + color +
                ", talla=" + talla +
                ", descripcion='" + descripcion + '\'' +
                ", precio=" + precio +
                '}';
    }

}
