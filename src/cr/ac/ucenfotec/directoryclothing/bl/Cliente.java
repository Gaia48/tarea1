package cr.ac.ucenfotec.directoryclothing.bl;

import java.util.Objects;

public class Cliente {

    //Variables

    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String direccion;
    private String correo;
    private String metodoPago;

    //Constructor que no recibe parametros



    public Cliente() {
        setNombre("");
        setPrimerApellido("");
        setSegundoApellido("");
        setDireccion("");
        setCorreo("");
        setMetodoPago("");
    }


    //Constructor que recibe parametros


    public Cliente(String nombre, String primerApellido, String segundoApellido, String direccion, String correo, String metodoPago) {
        setNombre(nombre);
        setPrimerApellido(primerApellido);
        setSegundoApellido(segundoApellido);
        setDireccion(direccion);
        setCorreo(correo);
        setMetodoPago(metodoPago);
    }

    //Getters & Setters


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getMetodoPago() {return metodoPago;}

    public void setMetodoPago(String metodoPago){this.metodoPago= metodoPago;}

    @Override
    public String toString() {
        return "Cliente{" +
                "nombre='" + nombre + '\'' +
                ", primerApellido='" + primerApellido + '\'' +
                ", segundoApellido='" + segundoApellido + '\'' +
                ", direccion='" + direccion + '\'' +
                ", correo='" + correo + '\'' +
                ", metodoPago='" + metodoPago + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return Objects.equals(getCorreo(), cliente.getCorreo());
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
